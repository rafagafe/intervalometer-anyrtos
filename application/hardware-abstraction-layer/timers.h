
/* ------------------------------------------------------------------------ */
/** @file timer.h
  * @brief Interface to timer module.
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _TIMERS_
#define _TIMERS_

#include "anyRTOS.h"
#include "hal-cfg.h"

/** @defgroup timers Timers
  * This module controls the hardware timers so that tasks can make 
  * measurements of time.
  * @{ */ 



/** Configure this module and hardware timer. */
void timer_allInit( void );


unsigned int timer_status( timer_t const* timer );


#ifdef HAL_HAS_TIMER0

/** anyRTOS timer instance for timer 0. */
extern timer_t timer0;

/** Convert to timer 0 ticks a time in seconds. */
#define timer0_sec( x ) \
    ((tick_t)(HAL_TIMER0_FREQ*(x))?(tick_t)(HAL_TIMER0_FREQ*(x)):(tick_t)1)


#endif


#ifdef HAL_HAS_TIMER1

/** anyRTOS timer instance for timer 1. */
extern timer_t timer1;

/** Convert to timer 1 ticks a time in seconds. */
#define timer1_sec( x ) \
    ((tick_t)(HAL_TIMER1_FREQ*(x))?(tick_t)(HAL_TIMER1_FREQ*(x)):(tick_t)1)

#endif


/** @} */

#endif /* _TIMERS_ */

