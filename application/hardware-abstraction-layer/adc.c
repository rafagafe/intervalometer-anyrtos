
/* ------------------------------------------------------------------------ */
/** @file adc.c
  * @brief This module implements methods to get information from ADC .
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include "hal-cfg.h"

#ifdef HAL_HAS_ADC

#include <msp430.h>
#include "anyRTOS.h"

static event_t _endOfConversion;
static mutex_t _busy;

/* Initilizes this module. */
void adc_init( void ) {
    event_init( &_endOfConversion );
    mutex_init( &_busy );
}

/* Starts an ADC conversion and wait until the converion finishes. */
uint16_t adc_get( unsigned channel ) {
    mutex_enterCritical( &_busy );
    channel <<= 12;
    ADC10CTL0 = ADC10SHT_3 | ADC10ON | ADC10IE;
    ADC10CTL1 = ADC10SSEL_0 + channel;
    ADC10CTL0 |= ENC | ADC10SC; 
    event_wait( &_endOfConversion );
    uint16_t result = ADC10MEM;
    mutex_exitCritical( &_busy );
    return result;      
}

/** ADC ISR: */  
__attribute__( ( __interrupt__( ADC10_VECTOR ) ) ) 
static void _isr( void ) {
    ADC10CTL0 &= ~ADC10ON;
    event_notify( &_endOfConversion );    
}

#endif /* HAL_HAS_ADC */

