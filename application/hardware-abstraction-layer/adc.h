
/* ------------------------------------------------------------------------ */
/** @file adc.h
  * @brief Interface to ADC module.
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _ADC_
#define _ADC_

#include "hal-cfg.h"

/** @defgroup adc ADC
  * This module implements methods to get information from ADC.
  * @{ */ 

/** Initilizes this module. */
void adc_init( void );

/** Starts an ADC conversion and wait until the converion finishes.
  * @param channel: The ADC channel.
  * @return The conversion in raw value. */
uint16_t adc_get( unsigned channel );

/** Waits until get the CPU temperature.
  * @return Grades centigrades.*/
static inline int16_t adc_getTemp( void ) {
    return -278 + adc_get( 10 );   
}

/** @} */

#endif /* _ADC_ */