
/* ------------------------------------------------------------------------ */
/** @file  prompt.h
  * @date   04/11/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _PROMPT_
#define _PROMPT_

/** @defgroup prompt Prompt
  * @{ */

/** Receives commands by serial port. */
void prompt_task( void* param );

/** @} */

#endif /* _PROMPT_ */
