
/* ------------------------------------------------------------------------ */
/** @file  anyRTOS-conf.h
  * 
  * @date   04/11/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _ANYRTOS_CONF_
#define _ANYRTOS_CONF_

/** Defines the number of priorities. */
#define ANYRTOS_PRIORYTIES_QTY    3

/** Remove some features for a better performance. */
#define ANYRTOS_BASIC_MODE        1

/** Application uses QUEUE */
#define ANYRTOS_USE_QUEUE         1

#endif /* _ANYRTOS_CONF_ */
