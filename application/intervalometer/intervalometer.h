
/* ------------------------------------------------------------------------ */
/** @file  intervalometer.h
  * @date   04/11/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _INTERVALOMETER_
#define _INTERVALOMETER_

#include "anyRTOS.h"

/** @defgroup interval Intervalometer
  * @{ */

/** Initializes this module.
  * @param th: The thread handler of intervalometer_task(). */
void interval_init( thread_t* th );

/** Enter the intervalometer settings by terminal. */
void interval_enterSettings( void );

/** Print the the intervalometer settings. */
void interval_printSettings( void );

/** Try to run a intervalometer process. */
void interval_run( void );

/** Try to run a HDR process. */
void interval_hdr( void );

/** Stop a running process. */
void interval_stop( void );

/** Check the task interval timer is running some process. */
bool intervalometer_isRunning( void ) ;

void intervalometer_printSpeeds( void );

/** Thread that runs the intervalometer and HDR processes. */
void intervalometer_task( void* param );

/** @} */

#endif /* _INTERVALOMETER_ */