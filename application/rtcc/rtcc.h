
/* ------------------------------------------------------------------------ */
/** @file  rtcc.h
  * @date   04/11/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _RTCC_
#define _RTCC_

/** @defgroup rtcc Real Time Clock Calendar
  * @{ */

/** Initializes this module. */
void rtcc_init( void );

/** Sets the real time clock calendar by terminal. */
void rtcc_enterTime( void );

/** Stops the real time clock calendar. */
void rtcc_stop( void );

/** Print the time and date by terminal. */
void rtcc_print( void );

/** Sets an alarm by terminal.
  * @param func: callback function. */
void rtcc_enterAlarm( void(*func)(void) );

/** Unsets the alarm. */
void rtcc_alarmOff( void );

/** Prints the date-time of alarm if it is set.
  * @return The pointer to callback or null if alarm is not set. */
void* rtcc_printAlarm( void );

/** Thread that runs the real time clock calendar. */
void rtcc_task( void* param );

/** @} */

#endif	/* _RTCC_ */
