
/* ------------------------------------------------------------------------ */
/** @file  join.h  
  * @date   04/11/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _JOIN_
#define	_JOIN_

#include <stdbool.h>
#include "anyRTOS-conf.h"
#include "timer.h"
#include "src/thread-list.h"

#ifdef __cplusplus
extern "C" {
#endif
    
/** @defgroup join Join Threads
  * @{ */ 

/** Structure to handle joins. */
typedef struct join_s {
    priorList_t list;
    unsigned char qty;
    unsigned char state;
} join_t;

/** Initializes a join. 
  * @param join: Join handler.
  * @param qty: Number of threads to be joined. */
void join_init( join_t* join, unsigned char qty );

/** Waits until join n threads. n is defined by join_init().
  * @param join: Join handler. */
void join_wait( join_t* join ) ;

#if !defined( ANYRTOS_BASIC_MODE ) || ( !ANYRTOS_BASIC_MODE )

/** Waits until join n threads or until the tick counter of a 
  * timer gets the task tick. n n is defined by join_init().
  * @param join: Join handler.
  * @param timer: Timer handler.
  * @retval true:  The join occurs before the timer gets the task tick.
  * @retval false: The timer gets the task tick before the join occurs. */
bool joinTimer_wait( join_t* join, timer_t* timer );

#else

/** Waits until join n threads. n is defined by join_init().
  * @param join: Join handler.
  * @param timer: Timer handler, ignored.
  * @return It always returns true. */
static inline bool joinTimer_wait( join_t* join, timer_t* timer ) {
    (void)timer;
    join_wait( join );
    return true;     
    
}

#endif /* ANYRTOS_BASIC_MODE */

/** @} */ 

#ifdef __cplusplus
}
#endif

#endif	/* _JOIN_ */

