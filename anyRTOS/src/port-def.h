
/* ------------------------------------------------------------------------ */
/** @file  port-def.h
  * 
  * @date   04/11/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _PORTABLE_DEF_
#define _PORTABLE_DEF_

#ifdef __MSP430__

#include <setjmp.h>

/** Attribute to increase the effectiveness of the threads. */
#define thread __attribute__(( naked ))

/** Type for stack memory. */
typedef unsigned int stack_t;

/** Type for timer tick. */
typedef unsigned int tick_t;

/** Structure with data portable in threads. */
typedef struct port_s {
    jmp_buf context; /**< MCU context */
} port_t;

#else

#error "Unknown MCU" 

#endif

#endif /* _PORTABLE_DEF_ */
