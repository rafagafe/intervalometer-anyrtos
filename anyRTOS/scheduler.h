
/* ------------------------------------------------------------------------ */
/** @file  scheduler.h  
  * @date   04/11/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _SCHEDULER_
#define	_SCHEDULER_

#include <stdint.h>
#include <stddef.h>
#include "src/thread-list.h"

#ifdef __cplusplus
extern "C" {
#endif
    
/** @defgroup scheduler Scheduler Control
  * @{ */

/** Initializes the scheduler. */
void scheduler_init( void );

/** Information for adding threads. */
typedef struct threadInfo_s {
    thread_t* th;          /**< Thread handler. */
    void(*process)(void*); /**< Pointer to thread function. */
    void* param;           /**< Parameter for thread. */
    stack_t* stack;        /**< Pointer to stack. */
    size_t size;           /**< Size in bytes of stack. */
    prior_t prior;         /**< Priority of thread. 0 is the highest. */
} threadInfo_t;

/** Adds a new thread to scheduler. */
void scheduler_add( threadInfo_t const* info );

/** Starts the scheduler. */
void scheduler_run( void );

/** @} */             

#ifdef __cplusplus
}
#endif

#endif	/* _SCHEDULER_ */

