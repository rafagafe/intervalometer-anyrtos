#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=msp430-gcc
CCC=msp430-g++
CXX=msp430-g++
FC=gfortran
AS=msp430-as

# Macros
CND_PLATFORM=msp430-Linux
CND_DLIB_EXT=so
CND_CONF=Simulation
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/57ce514c/queue.o \
	${OBJECTDIR}/_ext/7eb4f418/anyRTOS.o \
	${OBJECTDIR}/_ext/f63e007e/date-time.o \
	${OBJECTDIR}/_ext/ac721013/adc.o \
	${OBJECTDIR}/_ext/ac721013/board-msp-exp430g2.o \
	${OBJECTDIR}/_ext/ac721013/serial-port.o \
	${OBJECTDIR}/_ext/ac721013/timers.o \
	${OBJECTDIR}/_ext/4125bdcd/intervalometer.o \
	${OBJECTDIR}/_ext/37a6c781/main.o \
	${OBJECTDIR}/_ext/dfd12fb2/prompt.o \
	${OBJECTDIR}/_ext/136aa3f0/rtcc.o \
	${OBJECTDIR}/_ext/e5338d82/switch.o


# C Compiler Flags
CFLAGS=-mmcu=msp430g2553 -ffunction-sections

# CC Compiler Flags
CCFLAGS=-mmcu=msp430g2553
CXXFLAGS=-mmcu=msp430g2553

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/intervalometer.elf

${CND_DISTDIR}/${CND_CONF}/intervalometer.elf: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/intervalometer.elf ${OBJECTFILES} ${LDLIBSOPTIONS} -Wl,--gc-sections

${OBJECTDIR}/_ext/57ce514c/queue.o: ../anyRTOS-util/queue.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/57ce514c
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/57ce514c/queue.o ../anyRTOS-util/queue.c

${OBJECTDIR}/_ext/7eb4f418/anyRTOS.o: ../anyRTOS/src/anyRTOS.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/7eb4f418
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/7eb4f418/anyRTOS.o ../anyRTOS/src/anyRTOS.c

${OBJECTDIR}/_ext/f63e007e/date-time.o: ../application/date-time/date-time.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/f63e007e
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f63e007e/date-time.o ../application/date-time/date-time.c

${OBJECTDIR}/_ext/ac721013/adc.o: ../application/hardware-abstraction-layer/adc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/ac721013
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ac721013/adc.o ../application/hardware-abstraction-layer/adc.c

${OBJECTDIR}/_ext/ac721013/board-msp-exp430g2.o: ../application/hardware-abstraction-layer/board-msp-exp430g2.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/ac721013
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ac721013/board-msp-exp430g2.o ../application/hardware-abstraction-layer/board-msp-exp430g2.c

${OBJECTDIR}/_ext/ac721013/serial-port.o: ../application/hardware-abstraction-layer/serial-port.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/ac721013
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ac721013/serial-port.o ../application/hardware-abstraction-layer/serial-port.c

${OBJECTDIR}/_ext/ac721013/timers.o: ../application/hardware-abstraction-layer/timers.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/ac721013
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ac721013/timers.o ../application/hardware-abstraction-layer/timers.c

${OBJECTDIR}/_ext/4125bdcd/intervalometer.o: ../application/intervalometer/intervalometer.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/4125bdcd
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4125bdcd/intervalometer.o ../application/intervalometer/intervalometer.c

${OBJECTDIR}/_ext/37a6c781/main.o: ../application/main.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/37a6c781
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/37a6c781/main.o ../application/main.c

${OBJECTDIR}/_ext/dfd12fb2/prompt.o: ../application/prompt/prompt.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/dfd12fb2
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/dfd12fb2/prompt.o ../application/prompt/prompt.c

${OBJECTDIR}/_ext/136aa3f0/rtcc.o: ../application/rtcc/rtcc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/136aa3f0
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/136aa3f0/rtcc.o ../application/rtcc/rtcc.c

${OBJECTDIR}/_ext/e5338d82/switch.o: ../application/switch/switch.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/e5338d82
	${RM} "$@.d"
	$(COMPILE.c) -g -O -Wall -D__MSP430G2553__ -I../anyRTOS -I../anyRTOS-util -I../application -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/e5338d82/switch.o ../application/switch/switch.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/intervalometer.elf

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
